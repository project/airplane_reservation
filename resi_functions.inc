<?php

/**
 * resi.module : resi.inc
 * Base functions
 */
 
function resi_get_topmenu($title="")
{
  $content .= '<center><b><a href="?q=resi">&Uuml;bersicht</a> | <a href="?q=resi/cal">Kalender</a> | <a href="?q=resi/aed/a">neue Reservierung eintragen</a></b></center>';
  
  if (!empty($title))
  $content .= '<h2>'.$title.'</h2>';
 
  return $content;
}
 
function resi_get_planes()
{
  $a = mysql_query('SELECT * from '.RESI_TAB_PLANE);
  $planes = array();
  while($b = mysql_fetch_row($a)) {
     
     $planes[] = $b[2];
  }
  
  return $planes;
}
 
 
// liefert eine Tabelle mit allen Reservierungen in einer Woche, angefangen mit dem �bergebenen Tag (timestamp)
function resi_get_week($stamp) 
{  
  global $user;
  $ST = "";

$planes = resi_get_planes();
  
  $tage = array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
   
  $ST .= "<table width=100% border=0 cellpadding=0 cellspacing=0 style=\"empty-cells:show;border=0px #000 solid\">";
  
  $tstoday = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));

  for ($i = 0; $i < 7; $i++)
  { 
    $thisday_stamp = mktime(0, 0, 0, date("m", $stamp ), date("d", $stamp )+$i, date("Y", $stamp ));
    $thisday = date("Y-m-d", $thisday_stamp);

  $day_in_year = date("z", $thisday_stamp);
  
    $Sunrise =  calcSunset($thisday_stamp, 50.4046, 8.1022, 0, 0);
    $Sunset =  calcSunset($thisday_stamp, 50.4046, 8.1022, 1, 0);

//  $Sunrise = date_sunrise($thisday_stamp, SUNFUNCS_RET_STRING, 50.4046, 8.1022, 90, 0);
//  $Sunset = date_sunset($thisday_stamp, SUNFUNCS_RET_STRING, 50.4046, 8.1022, 90, 0);


    if ($thisday_stamp == $tstoday)
       $ST .= "<tr bgcolor=\"#A0A0A0\">";
    else
       $ST .= "<tr bgcolor=\"#D0D0D0\">";

    $ST .= "<th align=left>" . date("d", $thisday_stamp)." ".$tage[date("w", $thisday_stamp)];
    
    $ST .= " (SR: ".$Sunrise.", SS: ".$Sunset." UTC)";
    
    $ST .= "<th align=right>";
    $ST .= "<a href=\"".RESI_LINK_PATH."aed/a/".$thisday."\"><img src=\"".RESI_PIC_PATH."new.png\" align=middle alt=\"neu\"></a>&nbsp;";
    $ST .= "<tr><td colspan=2>";

    $CONT = "";
    $j = 0;

    // trickreiche Abfrage, liefert die Pilotennamen mit deren Reservierungen f�r einen Tag
    $query  = 'SELECT '.RESI_TAB_RESERVIERUNG.'.plane, '.RESI_TAB_RESERVIERUNG.'.start, '.RESI_TAB_RESERVIERUNG.'.end, ';
    $query .= RESI_TAB_RESERVIERUNG.'.comment, '.RESI_TAB_RESERVIERUNG.'.id, ';
    $query .= RESI_TAB_RESERVIERUNG.'.pilot, '.RESI_TAB_RESERVIERUNG.'.bookfor, ';
    $query .= RESI_TAB_USERS.'.name, '.RESI_TAB_USERS.'.mail ';
    $query .= ' FROM '.RESI_TAB_RESERVIERUNG.' INNER JOIN '.RESI_TAB_USERS;
    $query .= ' WHERE '.RESI_TAB_RESERVIERUNG.'.pilot = '.RESI_TAB_USERS.'.uid ';
    $query .= ' AND ((LEFT('.RESI_TAB_RESERVIERUNG.'.start, 10)="'.$thisday.'" OR LEFT('.RESI_TAB_RESERVIERUNG.'.end, 10)="'.$thisday.'")';
    $query .= ' OR (LEFT('.RESI_TAB_RESERVIERUNG.'.start, 10)<"'.$thisday.'" AND LEFT('.RESI_TAB_RESERVIERUNG.'.end, 10)>"'.$thisday.'"))';
    $query .= ' ORDER BY '.RESI_TAB_RESERVIERUNG.'.start ASC';
    $result= mysql_query($query) or die ("#wk1: Anfrage fehlgeschlagen: " . mysql_error());

    while($flight = mysql_fetch_assoc($result)) 
    {
/*     $k=0;
      while ($k < count ($planes) && $planes[$k][0] != $flight["plane"])
      {
        $k++;
      }
/**/
      $sday = substr($flight["start"],0,10);
      $stime = substr($flight["start"],11,5);
      $eday = substr($flight["end"],0,10);
      $etime = substr($flight["end"],11,5);
      
      if ((($sday != $thisday) && ( $eday != $thisday)) || (($sday == $eday) && ($stime == "00:00") && ($etime == "23:59") ))
        $CONT .= "ganzt&auml;gig";
      else
      {          
        // heute oder gestern gestartet?
        if ($sday != $thisday)
          $CONT .= "...";
        else  
          $CONT .= $stime;
        
        $CONT .= "-";
        
        // heute oder morgen gelandet?
        if ($eday != $thisday)
          $CONT .= "...";
        else  
          $CONT .= $etime ;
      }

      $CONT .= " ". $planes[$flight["plane"]];

  
    if (empty($flight["bookfor"])) 
      $name =$flight["name"];
    else
      $name =$flight["bookfor"];
    
    if (empty($flight["bookfor"])) // selbst erstellt
    {     
      $CONT .= " <a href=\"?q=user/".$flight["pilot"]."\">".$name."</a>";
      //$CONT .= " <a href=\"mailto:". $flight["mail"]."\">".$name."</a>";
    }
    else // erstellt von turmfralken
    { 
      $CONT .= "&nbsp;".$flight["bookfor"]."<img src=\"".RESI_PIC_PATH."star.png\" alt=\"gebucht von ".$flight["name"]."\" title=\"gebucht von ".$flight["name"]."\">";
    }
    
  
    if ($flight["comment"] != "") 
      $CONT .= " (".$flight["comment"].")";

    if (($user->uid == $flight["pilot"]) || is_resi_mod())
    {
      $CONT .= "&nbsp;<a href=\"".RESI_LINK_PATH."aed/e/".$flight["id"]."\">";
      $CONT .= "<img src=\"".RESI_PIC_PATH."edit.png\" align=middle alt=\"�ndern\" title=\"�ndern\"></a>";
    }
    else 
      $restable .= "<td>&nbsp;";
  
    $CONT .= "<br>";

    } // while

    $ST .= $CONT;
    $ST .= "&nbsp;";

  } //  for ($i = 0; $i < 7; $i++)
  
  $ST .= "</table>";

  return $ST;
}

function resi_make_calendar($starttag) {

  $monate = array(1=>"Januar", 2=>"Februar", 3=>"M&auml;rz", 4=>"April", 5=>"Mai", 6=>"Juni", 7=>"Juli", 8=>"August", 9=>"September", 10=>"Oktober",  11=>"November", 12=>"Dezember");
  
  $content = "";
  
  $pieces = explode ("-", $starttag);
  $starttag_stamp = mktime(0, 0, 0, $pieces[1]  ,  $pieces[2], $pieces[0]);

  // wenn keine Wochennr. angegeben, heutige Woche w�hlen
  if  ($starttag == 0)
  {
    $tsToday = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
    $starttag_stamp = $tsToday;
     
    # count down to monday
    while (strftime('%u', $starttag_stamp) != 1)
           $starttag_stamp -= 60*60*24;
  }
  
  // Navigationspunkte berechnen:
  $tag = date("d", $starttag_stamp);
  $monat = date("m", $starttag_stamp);
  $jahr = date("Y", $starttag_stamp);
  
  $starttag = date("Y-m-d", $starttag_stamp);
  $lastweek = date("Y-m-d", (mktime(0, 0, 0, $monat  , $tag-7, $jahr)));
  $nextweek = date("Y-m-d", (mktime(0, 0, 0, $monat  , $tag+7, $jahr)));

  $lastmonth = date("Y-m-d", (mktime(0, 0, 0, $monat  , $tag-28, $jahr)));
  $nextmonth = date("Y-m-d", (mktime(0, 0, 0, $monat  , $tag+28, $jahr)));

  $endmonat = date("n", mktime(0, 0, 0, $monat  , $tag+6, $jahr));

  $monat = date("n", $starttag_stamp); 

  $content .= '<table width=100% border=0>';
  $content .= '<tr><td align=left><a href="'.RESI_LINK_PATH.'cal/'.$lastweek.'"><img src="'.RESI_PIC_PATH.'prev.gif" align=middle alt="vorherige Woche"> vorherige Woche</a>'; 
  $content .= '<td align=center>';

  $content .= '<a href="'.RESI_LINK_PATH.'cal/'.$lastmonth.'"><img src="'.RESI_PIC_PATH.'prev.gif" align=middle alt="vorheriger Monat"></a> '; 

  $content .= '<b>'.$monate[$monat];
  
  if ($endmonat != $monat)
    $content .= "/".$monate[$endmonat];
  
  $content .= ' '.$jahr . ' </b>';
  $content .= ' <a href="'.RESI_LINK_PATH.'cal/'.$nextmonth.'"><img src="'.RESI_PIC_PATH.'next.gif" align=middle alt="n�chster Monat"></a>'; 
  
  $content .= '<td align=right>';
  $content .= '<a href="'.RESI_LINK_PATH.'cal/'.$nextweek.'">n&auml;chste Woche <img src="'.RESI_PIC_PATH.'next.gif" align=middle alt="n�chste Woche"></a>';
  $content .= '</table>';

  // und jetzt noch den Hauptteil: die Wochen�bersicht (aus functions.php)
  $content .= resi_get_week($starttag_stamp);

  $content .= "<br><br><img src=\"".RESI_PIC_PATH."star.png\"\"> Reservierung erstellt von Turmfalken oder "
       .  "Administrator f&uuml;r den jeweiligen Piloten.<br>Mauszeiger &uuml;ber den Stern halten, um anzuzeigen, wer diese Reservierung eingetragen hat.";

  
  
  return $content;
} 
 
// liefert komplette Tabelle

function get_settings() {
	global $max_reservations;
  global $max_weeks;
  global $check_pilotdata;
  global $CONFIG;
  global $TAB_CONFIG;
  
     $a = mysql_query('SELECT * FROM '.$TAB_CONFIG) or die ("#LDCFG1: Anfrage fehlgeschlagen: " . mysql_error());
     $settings = array();
     while($b = mysql_fetch_row($a)) {
         $settings[] = $b;
     }

  $max_reservations = $settings[$CONFIG['max num of reservations']][1];
  $max_weeks = $settings[$CONFIG['max weeks in advance']][1];
  $check_pilotdata = $settings[$CONFIG['check pilot data']][1];
}
// liefert komplette Tabelle
function resi_mysql_table($query) {
     $a = mysql_query($query);
     $c = array();
     while($b = mysql_fetch_row($a)) {
         $c[] = $b;
     }
     return $c;
 }



// liefert die News aus der richtigen Kategorie
function resi_get_news() {

  $s = "";
  $s .= "<a href=\"modules.php?op=modload&name=Submit_News&file=index\">neuen Beitrag einreichen</a>";
  $s .= "<br /><br />";

  $query = 'SELECT pn_title, pn_time, pn_hometext, pn_informant FROM nuke_stories WHERE pn_topic=3 ORDER BY pn_time DESC';
  $result= mysql_query($query); // Query ausf�hren

  while ($row = mysql_fetch_row($result)) 
  {
    $s .= "<br /><span class=\"pn-title\">".$row[0]."</span><br />";
    //$s .= "<span class=\"pn-normal\">Von ".$row[3]." am ".$row[1];
    $s .= "<br /><br />".$row[2]."</span>";
    $s .= "<br /><br />";
  }
  
  return $s;
}




// erstellt eine Tabelle der Reservierungen aus �bergebenem SQL-Befehl
function resi_get_reservations_table($where, $titel = "") {
  global $uid;
  global $ModName;
  global $TAB_PLANE;
  global $planes;
  global $TAB_RESERVIERUNG;
  global $TAB_NUKE_USERS;
  global $moderator;

  $background1= "#CCC";
  $background2= "#EEE";
  
  $restable = "";
  
  $query  = 'SELECT id, plane, pilot, start, end, comment, plt.pn_uname, plt.pn_email, plt.pn_name, mod';
  $query .= ' FROM '.$TAB_RESERVIERUNG;
  $query .= ' LEFT JOIN '.$TAB_NUKE_USERS.' AS plt ON plt.pn_uid=pilot';
  $query .= ' WHERE '.$where;

  if (!empty($titel))
    $restable .= "<span class=\"pn-title\">".$titel."</span><br><br>";
    
  $restable .= "<table border=\"1\" rules=\"groups\" style=\"background:#AAA;\" width=100% >";
  $restable .= "<thead><tr><th colspan=2>Flugzeug<th colspan=2 style=\"border-left:1px solid #000;\">von<th colspan=2 style=\"border-left:1px solid #000;\">bis<th align=left  width=150px; style=\"border-left:1px solid #000;\">&nbsp;Reserviert von<th align=left>Kommentar<th>&nbsp;</thead><tbody>";
   
  //echo $query;
  $result= mysql_query($query); // Query ausf�hren
  
  $i=0;
  while ($row = mysql_fetch_row($result)) 
  {
		$res_userid=$row[2];
    // Flugzeug-Daten im Array suchen
    $j = 0;
    while ($j < count ($planes) && $planes[$j][0] != $row[1])
     $j++;
     
    // Datum und Uhrzeit sch�n formatieren
    $sdate = date("j.n.y",  strtotime($row[3]));
    $stime = date("H:i",  strtotime($row[3]));
    $ldate = date("j.n.y",  strtotime($row[4]));
    $ltime = date("H:i",  strtotime($row[4]));
     
    $i++;
      if ($i & 1)
        $restable.="\n<tr align=center style=\"background-color:".$background1."\">";
      else
        $restable.="\n<tr align=center style=\"background-color:".$background2."\">";

    $restable .= "<td width=60px;>".$planes[$j][1]."<td width=80px;>".$planes[$j][2];
    
    $restable .="<td width=60px; style=\"border-left:1px solid #000;\">".$sdate."<td width=50px; >".$stime;
    $restable .="<td width=60px; style=\"border-left:1px solid #000;\">".$ldate."<td width=50px; >".$ltime;
    
    $restable .="<td  width=140px; align=left style=\"border-left:1px solid #000;\">&nbsp;";

		if (empty($row[8]))
	    $name =$row[6];
	  else
	    $name =$row[8];

 		if (empty($row[9])) // selbst erstellt
		{ 		
	    $restable .="<a href=\"mailto:$row[7]\"><img src=\"".RESI_PIC_PATH."mail.png\" align=middle alt=\"Email\" title=\"Email\"></a> "
	              . "<a href=\"http://fsv.wendebaum.de/user.php?op=userinfo&uname=".$row[6]."\">".$name."</a>";
		}
		else // erstellt von mod
		{ 
				$restable .=$row[9]."<img src=\"modules/$ModName/pnimages/star.png\" alt=\"gebucht von ".$name."\" title=\"gebucht von ".$name."\">";
		}
		
    $restable .="<td align=left>".$row[5];
              
    if (($uid == $res_userid) || $moderator)
    {
      $restable .= "<td>";
      $restable .= "&nbsp;<a href=\"modules.php?op=modload&amp;name=Resi&amp;file=index&amp;action=add&amp;id=".$row[0]."\">";
      $restable .= "<img src=\"modules/$ModName/pnimages/edit.png\" alt=\"�ndern\" title=\"�ndern\"></a>";
    }
    else 
      $restable .= "<td>&nbsp;";
  }
  $restable .= "</tbody></table>";
  return $restable;
}



?>